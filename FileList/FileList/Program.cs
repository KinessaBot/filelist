﻿using System;
using System.Threading.Tasks;
using System.IO;
using System.Text;

namespace FileList
{
    class Program
    {
        static string pPath = "c:\\";
        static void Main()
        {
            string commandLine;
            ShowPPath();
            while ((commandLine = Console.ReadLine()) != String.Empty)
            {
                var args = commandLine.Split(" ");
                switch (args[0].ToUpper())
                {
                    case "CD": CD(args); break;
                    case "OP": OP(args); break;
                    default: Console.WriteLine("Invalid command"); break;
                }
                ShowPPath();
            }
        }

        private static void CD(string[] args)
        {
            try
            {
                CheckArgCount(args, 3);
                if (args.Length == 1)
                    ShowFS(new DirectoryInfo(pPath), ReadMode.B);
                else
                {
                    ReadMode rm = ReadMode.B;
                    if (args.Length == 3)
                        switch (args[2].ToUpper())
                        {
                            case "F": rm = ReadMode.F; break;
                            case "B": rm = ReadMode.B; break;
                            case "D": rm = ReadMode.D; break;
                            default: Console.WriteLine("Invalid read mode"); return;
                        }
                    DirectoryInfo dire = new DirectoryInfo(args[1]);
                    string iPath = Path.Combine(pPath, args[1]);
                    DirectoryInfo dire1 = new DirectoryInfo(iPath);
                    if (dire.Exists)
                    {
                        ShowFS(dire, rm);
                        pPath = args[1];
                    }
                    else
                    if (dire1.Exists)
                    {
                        ShowFS(dire1, rm);
                        pPath = iPath;
                    }
                    else Console.WriteLine("Unexisting directory");
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                Console.WriteLine("Too many arguments passed. This command supports maximum 2 arguments");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Cannot read the directory. " + ex.Message);
            }
        }

        private static void ShowFS(DirectoryInfo dire, ReadMode rm)
        {
            Console.WriteLine("\u0020");
            switch (rm)
            {
                case ReadMode.B:
                    FileSystemInfo[] fileSystems = dire.GetFileSystemInfos();
                    foreach (FileSystemInfo fs in fileSystems)
                        Console.WriteLine(fs.ToString());
                    break;
                case ReadMode.F:
                    FileInfo[] files = dire.GetFiles();
                    foreach (FileInfo fs in files)
                        Console.WriteLine(fs.ToString());
                    break;
                case ReadMode.D:
                    DirectoryInfo[] directoryInfos = dire.GetDirectories();
                    foreach (DirectoryInfo fs in directoryInfos)
                        Console.WriteLine(fs.ToString());
                    break;
            }
        }

        private static void CheckArgCount(string[] args, int argc)
        {
            if (args.Length > argc)
                throw new ArgumentOutOfRangeException();
        }

        private static void ShowPPath()
        {
            Console.Write(pPath + ">>>\t");
        }

        private enum ReadMode
        {
            F = 0,
            D,
            B
        }

        private static void OP(string[] args)
        {
            try
            {
                FileInfo file = new FileInfo(args[1]);
                FileInfo file1 = new FileInfo(Path.Combine(pPath + args[1]));
                if (file.Exists)
                    ShowFile(file);
                else
                if (file1.Exists)
                    ShowFile(file1);
                else
                    Console.WriteLine("Invalid file name");

            }
            catch (ArgumentOutOfRangeException)
            {
                Console.WriteLine("Too many arguments passed. This command supports maximum 1 argument");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Cannot read the file. " + ex.Message);
            }
        }

        private static void ShowFile(FileInfo file)
        {
            using (FileStream fileStream = file.OpenRead())
            {
                byte[] data = new byte[fileStream.Length];
                for (long i = 0; i < fileStream.Length; i++)                 
                    data[i]=(byte)fileStream.ReadByte();
                Console.Write(Encoding.UTF8.GetString(data));
            }
        }
    }
}
